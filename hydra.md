Just some notes from my attempt at setting up Hydra.

## Setting up on NixOS

No need for manual database creation and all that; just ensure that your PostgreSQL service is running (`services.postgresql.enable = true;`), and then enable the Hydra service (`services.hydra.enable`). The Hydra service will need a few more options to be set up, below is my configuration for it:

```
    services.hydra = {
        enable = true;
        port = 3333;
        hydraURL = "http://localhost:3333/";
        notificationSender = "hydra@cryto.net";
        useSubstitutes = true;
        minimumDiskFree = 20;
        minimumDiskFreeEvaluator = 20;
    };
```

Database and user creation and all that will happen automatically. You'll only need to run `hydra-init` and then `hydra-create-user` to create the first user. Note that you *may* need to run these scripts as root if you get permission or filesystem errors.

## Can't run `hydra-*` utility scripts / access the web interface due to database errors

If you already have a `services.postgresql.authentication` configuration line from elsewhere (either another service, or your own `configuration.nix`), it may be conflicting with the one specified in the Hydra service. There's an open issue about it [here](https://github.com/NixOS/nixpkgs/issues/32063).

## Can't login

After running `hydra-create-user` in your shell, you may be running into the following error in the web interface: "Bad username or password."

When this occurs, it's likely because the `hydra-*` utility scripts stored your data in a local SQLite database, rather than the PostgreSQL database you configured. As far as I can tell, this happens because of some missing `HYDRA_*` environment variables that are set through `/etc/profile`, which is only applied on your next login. Simply opening a new shell is not enough.

As a workaround until your next login/boot, you can run the following to obtain the command you need to run to apply the new environment variables in your current shell:

```sh
cat /etc/profile | grep set-environment
```

... and then run the resulting command (including the dot at the start, if there is one!) in the shell you intend to run the `hydra-*` scripts in. If you intend to run them as root, make sure you run the `set-environment` script *in the root shell* - using `sudo` will make the environment variables get lost, so you'll be stuck with the same issue as before.